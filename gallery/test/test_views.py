from django.test import TestCase
from django.urls import resolve, reverse

from ..models import Album, Photo
from ..views import AlbumListView, PhotoDetailView


class AlbumDetailView(TestCase):
    def setUp(self):
        Album.objects.create(title='Album_Test_1', summary='Photo Album 1.')

    def test_album_detail_view_success_status_code(self):
        url = reverse('album-detail', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_album_detail_view_not_found_status_code(self):
        url = reverse('album-detail', kwargs={'pk': 99})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

# for future ref
#class PhotoDetailView(TestCase):
#    def setUp(self):
#        Photo.objects.create(title='Photo_Test_1', summary='Picture 1.')
#
#    def test_photo_detail_view_success_status_code(self):
#        url = reverse('photo-detail', kwargs={'pk': 1})
#        response = self.client.get(url)
#        self.assertEquals(response.status_code, 200)
#
#    def test_photo_detail_view_not_found_status_code(self):
#        url = reverse('photo-detail', kwargs={'pk': 99})
#        response = self.client.get(url)
#        self.assertEquals(response.status_code, 404)
