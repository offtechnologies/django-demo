from django.test import TestCase
from gallery.models import Album, Photo


class AlbumModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        Album.objects.create(title='Album Title', summary='a brief description of the Album')

    def test_title_label(self):
        album=Album.objects.get(id=1)
        field_label = album._meta.get_field('title').verbose_name
        self.assertEquals(field_label,'title')

    def test_summary_label(self):
        album=Album.objects.get(id=1)
        field_label = album._meta.get_field('summary').verbose_name
        self.assertEquals(field_label,'summary')

    def test_title_max_length(self):
        album=Album.objects.get(id=1)
        max_length = album._meta.get_field('title').max_length
        self.assertEquals(max_length,200)

    def test_summary_max_length(self):
        album=Album.objects.get(id=1)
        max_length = album._meta.get_field('summary').max_length
        self.assertEquals(max_length,1000)

    def test_get_absolute_url(self):
        album=Album.objects.get(id=1)
        #This will also fail if the urlconf is not defined.
        self.assertEquals(album.get_absolute_url(),'/album/1')


class PhotoModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        #Set up non-modified objects used by all test methods
        Photo.objects.create(title='Photo Title', summary='a brief description of the Photo')

    def test_title_label(self):
        photo=Photo.objects.get(id=1)
        field_label = photo._meta.get_field('title').verbose_name
        self.assertEquals(field_label,'title')

    def test_summary_label(self):
        photo=Photo.objects.get(id=1)
        field_label = photo._meta.get_field('summary').verbose_name
        self.assertEquals(field_label,'summary')

    def test_title_max_length(self):
        photo=Photo.objects.get(id=1)
        max_length = photo._meta.get_field('title').max_length
        self.assertEquals(max_length,200)

    def test_summary_max_length(self):
        photo=Photo.objects.get(id=1)
        max_length = photo._meta.get_field('summary').max_length
        self.assertEquals(max_length,1000)

    def test_get_absolute_url(self):
        photo=Photo.objects.get(id=1)
        #This will also fail if the urlconf is not defined.
        self.assertEquals(photo.get_absolute_url(),'/photo/1')
