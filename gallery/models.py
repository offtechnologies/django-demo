from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFit
# Create your models here.

class Album(models.Model):
    """
    Model representing an Album.
    """
    title = models.CharField(max_length=200)
    summary = models.TextField(max_length=1000, help_text="Enter a brief description of the Album")
    cover = models.ImageField(upload_to='covers/',null=True, blank=True)
    cover_thumbnail = ImageSpecField(source='cover',
                                      processors=[ResizeToFit(400, 300)],
                                      format='JPEG',
                                      options={'quality': 100})
    cover_thumbnail_small = ImageSpecField(source='cover',
                                      processors=[ResizeToFit(200, 150)],
                                      format='JPEG',
                                      options={'quality': 80})
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        ordering = ['created']

    def get_absolute_url(self):
        """
        Returns the url to access a particular album instance.
        """
        return reverse('album-detail', args=[str(self.id)])

    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.title

class Photo(models.Model):
    """
    Model representing a Photo.
    """
    title = models.CharField(max_length=200)
    album = models.ForeignKey('Album', on_delete=models.SET_NULL, null=True)
    summary = models.TextField(max_length=1000, help_text="Enter a brief description of the Photo")
    picture = models.ImageField(upload_to='photos/',null=True, blank=True)
    picture_thumbnail = ImageSpecField(source='picture',
                                      processors=[ResizeToFit(400, 300)],
                                      format='JPEG',
                                      options={'quality': 100})
    picture_thumbnail_small = ImageSpecField(source='picture',
                                      processors=[ResizeToFit(200, 150)],
                                      format='JPEG',
                                      options={'quality': 80})
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    def get_absolute_url(self):
        """
        Returns the url to access a particular photo instance.
        """
        return reverse('photo-detail', args=[str(self.id)])

    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.title
