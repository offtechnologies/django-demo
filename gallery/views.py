from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views import generic
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Album, Photo

def home(request):
    """
    View function for home page of site.
    """
    # Number of visits to this view, as counted in the session variable.
    num_visits=request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits+1
    # Latest picture List
    latest_picture_list = Photo.objects.order_by('-created')[:15]
    # Render the HTML template home.html with the data in the context variable
    return render(
        request,
        'home.html',
        context={
            'num_visits':num_visits,
            'latest_picture_list':latest_picture_list,
        }
    )

class AlbumListView(generic.ListView):
    """
    Generic class-based list view for a list of albums.
    """
    model = Album
    paginate_by = 8

class AlbumDetailView(generic.DetailView):
    """
    Generic class-based detail view for an album.
    """
    model = Album
    #login_url = '/login/'

    def album_detail_view(request,pk):
        try:
            album_id=Album.objects.get(pk=pk)
        except Album.DoesNotExist:
            raise Http404("Album does not exist")

        return render(
            request,
            'album_detail.html',
            context={'album':album_id,}
            )

class PhotoListView(generic.ListView):
    """
    Generic class-based list view for a list of photos.
    """
    model = Photo
    paginate_by = 8


class PhotoDetailView(generic.DetailView):
    """
    Generic class-based detail view for a photo.
    """
    model = Photo

def photo_detail_view(request,pk):
    try:
        photo_id=Photo.objects.get(pk=pk)
    except Photo.DoesNotExist:
        raise Http404("Photo does not exist")
        photo_id=get_object_or_404(Photo, pk=pk)

    return render(
        request,
        'photo_detail.html',
        context={'photo':photo_id,}
    )
